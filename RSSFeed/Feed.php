<?php 

    $input = readline("Add new RSS or Show RSS feed? (show/add):\t");

    if($input === "show") {

        Read();
    }

    if($input === "add") {
    
        $current = "";

        do {

            $input = null;
            $input = readline("Add RSS:\t");

            $current .=$input.PHP_EOL;

            $input = null;
            $input = readline("Do you want add more? (no/yes):\t");

        } while(!$input === "no");  
        
        Write($current);
        Read();
                
    } 

    function Read() {

        echo "\n";
        $entries  = array();
        $links = file_get_contents("feeds.txt");
        $feeds = explode(",", $links);

        foreach($feeds as $feed) {

            $xml = simplexml_load_file($temp);
            $entries = array_merge($entries, $xml->xpath("//item"));
        }

        foreach($entries as $entry) {

                echo strftime('%I:%M %p', strtotime($entry->pubDate)) . " - ". $entry->title ."\n";
                $count++;                        
        }
    }

    function Write($input) {

        $links = file_get_contents("feeds.txt");
        $feeds = explode(",", $links);
        $temp = "";
        
        foreach($feeds as $feed) {

            $temp .= $feed.PHP_EOL;
        }

        $temp .= $input;

        file_put_contents("feeds.txt", $temp);
    }

?>